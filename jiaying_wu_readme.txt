*******************************************
Bazhuayu:
	This software need to installed in Windows Computer
	Before crawl Google Scholar, a textfile contain name need to be prepared.
	The sample textfile and instruction of how to crawl it is in the folder bazhuayu_instruction
*******************************************
Data Preparation:
	After Crawl the data, the data need to be seperated in three file:
		The cs_title.txt which only contains titles seperate by newline
		The cs_name_title file contains name of author '\t' title '\n'
		The cs_name_area file contains name of author '||' area1 '||area2' etc. '\n'
	Search NLP stanford, and put title file into the parser.
		seperate the file into format of cs_title_phrase
*******************************************
Title Labeling:
	run title_lable.py file, out the txt files in the corresponding place in title_lable.py
	make blank files: output_label.txt, title_label.txt, parse_area_prob.txt to store the result
	output_label.txt: phrase and its corresponding area
	title_label.txt：title and its corresponding area
	phrase_area_prob.txt：phrase and the probability of voting corresponding to the area