import re
import difflib
from collections import Counter
import operator
#read the textfile contains title and phrase pair
with open('cs_title_phrase.txt') as f:
    content = f.read().splitlines()
phrase_title={}
title_phrase={}
# construct a dictionary whose key is phrase and value is the titles that contains that phrase
for i in range(0,len(content)):
    title=content[i]
    if '|' in title:
        phrase_list = []
        words = title.split("|")
        for word in range(1,len(words)):
            key=words[word]
            if key in phrase_title.keys():
                phrase_title[key].append(words[0])
            else:
                titles=[]
                titles.append(words[0])
                phrase_title[key]=titles
            phrase_list.append(words[word])
        title_phrase[words[0]]=phrase_list
print('phrase title dictionary complete================')
f.close()

#============================
#Read the name title file
name_title=[]
with open('cs_name_title.txt') as f:
    content = f.read().splitlines()
count=0
for i in range(0,len(content)):
    text = content[i]
    words= text.split('\t')
    if i==0:
        prev_name=words[0]
        name_title.append(words)
        count+=1
        continue
    if count >40 and prev_name==words[0]:
        continue
    if count <= 40 and prev_name==words[0]:
        prev_name=words[0]
        name_title.append(words)
        count+=1
        continue
    if prev_name!=words[0]:
        count=0
        prev_name=words[0]
        name_title.append(words)
f.close()
#=====================================
title_name={}
temp='none'
for i in range(0,len(name_title)-1):
    title_name[name_title[i][1]]=name_title[i][0]
print('title name dictionary complete================')

#Construct a dictionary the key is title and the value is name of the author
#=====================================
#read the name area field
name_areas={}
with open('cs_name_area2.txt') as f:
    content = f.read().splitlines()

for i in range(0,len(content)):
#for i in range(0,10):
    title=content[i]
    if '||' in title:
        title = title.replace('\t', '')
        words = title.split("||")
        key=words[0]
        if key in name_areas.keys():
            continue
        else:
            areas=[]
            for word in range(1,len(words)):
                if words[word]!='':
                   areas.append(words[word])
            name_areas[key]=areas

#this is a dictionary the key is name of researcher and the value is the areas of that researcher
print('name area dictionary complete================')

#build the vote model by count number of repetition area count for a single phrase
phrase_area={}
phrase_area_prob={}
for phrase in phrase_title.keys():
    area_vote={}
    match_flag=0
    titles=phrase_title[phrase]
    for title in titles:
        if match_flag==1:
            break
        if title not in title_name.keys():
            continue
        author_name=title_name[title]
        if author_name not in name_areas.keys():
            continue
        areas=name_areas[author_name]
        for i in range(0,len(areas)):
            if areas[i].lower()==phrase.lower():
                phrase_area[phrase]=areas[i]
                phrase_area_prob[phrase]=0.9995 #if a phrase has same expression as an area A, we will label that phrase as area A
                match_flag=1
                break
            if areas[i] in area_vote.keys():#account for local matching
                if areas[i].lower() in title.lower():
                    area_vote[areas[i]]+=10
                else:
                    area_vote[areas[i]]+=1
            else:
                if areas[i].lower() in title.lower():
                    area_vote[areas[i]]=10
                else:
                    area_vote[areas[i]]=1
    if not area_vote:
        continue
    if match_flag==1:
        continue
    phrase_area[phrase]=max(area_vote,key=area_vote.get)
    phrase_area_prob[phrase]=0.0005+max(area_vote.values())/sum(area_vote.values())
    if phrase_area_prob[phrase]>1:
        phrase_area_prob[phrase]=1
    area_vote.clear()

import operator
#sort the label phrase dictionary by ascending confidence level
sort_phrase=sorted(phrase_area_prob.items(), key=operator.itemgetter(1))

#write it to a file
text_file = open("output_label_prob_sorted.txt", "w")
for pair in sort_phrase:
    key=pair[0]
    print(key)
    text_file.write(key + '\t' + ':'+'\t' +  phrase_area[key] + '\t'+ str(phrase_area_prob[key]) + '\n')
text_file.close()

#==========================================
#for the publication titles that can be parsed, lable it
phrases=[]
title_area={}
for key in title_phrase.keys():
    phrases=title_phrase[key]
    new_flag=1
    area_prob=0
    area='none'
    for phrase in phrases:
#        print(phrase)
        if phrase in phrase_area.keys():
            if new_flag==1:
                area=phrase_area[phrase]
                area_prob=phrase_area_prob[phrase]
                new_flag=0
            if new_flag==0:
                if phrase_area_prob[phrase]>area_prob:
                    area = phrase_area[phrase]
                    area_prob=phrase_area_prob[phrase]
    title_area[key]= area

text_file = open("title_label.txt","w")
for key in title_area.keys():
    if title_area[key]!='none':
        text_file.write(key + '\t' + ':' + '\t' + title_area[key] + '\n')
text_file.close()

#==============================for demonstration purpose
#for the title that cannot be parsed, label it by another method
#the following one is a trial, and have not been implemented into large scale data
stra='combing knowledge bases comsisting of first-order theories'
strb='Representing actions: Laws, observations and hypotheses'
auth_stra='Chitta Baral'
stra_words=stra.split()

area='none'
for word in stra_words:
    area_list1 = []
    for phrase in phrase_area.keys():
        if word in phrase:
            area_list1.append(phrase_area[phrase])
    area_list3 = list(set(area_list1) & set(name_areas[auth_stra]))
    for area in area_list3:
        if word in area:
            print(area)
    print(word)
    print(area_list3)

